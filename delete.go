package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
)

func Delete(loc string) {

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		if msg := scanner.Text(); msg != "main" {
			fmt.Printf("msg: %v\n", msg)
			switch loc {
			case "remote":
				cmd := exec.Command("git", []string{"push", "-d", "origin", msg}...)
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr
				if err := cmd.Run(); err != nil {
					log.Fatal(err)
				}
				fallthrough
			case "local":
				cmd := exec.Command("git", []string{"tag", "-d", msg}...)
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr
				if err := cmd.Run(); err != nil {
					log.Fatal(err)
				}
			}
		}

	}

}

func main() {
	Delete(os.Args[1])
}
