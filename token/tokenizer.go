package token

import (
	"strings"
)

func Hist(message *string) (hist map[string]int) {
	hist = map[string]int{}
	words := strings.FieldsFunc(*message, func(r rune) (approve bool) {
		approve = false
		if strings.ContainsRune(",. ;?", r) {
			approve = true
		}
		return
	})
	for _, word := range words {
		hist[word] += 1
	}
	return
}

func Version() string {
	return "v1.0.0"
}
